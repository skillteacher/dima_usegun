using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnemyAttack : MonoBehaviour
{
    [SerializeField] private float damage = 1f;
    [SerializeField] private anemyAnimation enemyAnimation;
    private  Transform  target;

    private void Start()
    {
        target = TargetsForEnemy.Instance.GetTarget();
    }


    private void OnEnable()
    {
        enemyAnimation.AnimationAttackMoment += AttackTarget;
    }
    private void OnDisable()
    {
        enemyAnimation.AnimationAttackMoment -= AttackTarget;
    }


    public void AttackTarget()
    {
        target.GetComponent<PlayerHealth>().TakeDamage(damage);
    }
}
