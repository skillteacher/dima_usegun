using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public event Message PlayerDeath;

    [SerializeField] private float healthAmount = 100f;

    public void TakeDamage(float damageamount)
    {
        healthAmount -= damageamount;

        if (healthAmount <= 0)
        {
            Debug.Log("���� �� � ���� ���� �����,�� �� ����");
            PlayerDeath?.Invoke ();

        }
    }
}