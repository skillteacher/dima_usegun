using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammo : MonoBehaviour
{
    [SerializeField] private int ammoAmount = 10;
    [SerializeField] private Text ammoText;

    private void Start()
    {
        ammoText.text = ammoAmount.ToString();
    }

    internal void AddAmmo(int ammoCount)
    {
        throw new NotImplementedException();
    }

    public void ReduceAmmo()
    {
        ammoAmount --;
    }

    public bool IsNoAmmo()
    {
        return ammoAmount <= 0;
    }
}
