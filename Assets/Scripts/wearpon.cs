using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wearpon : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1")) 
        {
            Shoot();
        }
    }
    private void Shoot()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range)) return;
        //Debug.Log("� ����� � "+ hit.transform.name);
        Health targetHealth = hit.transform.GetComponent<Health>();
        if (targetHealth == null) return;
        targetHealth.TakeDamage(damage);
    }
}
